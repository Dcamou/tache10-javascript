// 1. Demander a l'utilisateur de remplire deux tableaux (tab1 et tab2) dont la taille est 5
const tab1 = [5, 11, 7, 17, 9];
const tab2 = [4, 6, 7, 9, 11];
console.log(tab1, tab2);

//Creer tab3 a partir des elements du tab1 qui ne se trouvent pas dans tab2
const tab3 = tab1.filter(x => tab2.indexOf(x) === -1);
console.log(tab3);

//Creer tab4 a partir des elements du tab1 qui se trouvent dans tab2
const tab4 = tab1.filter(x => tab2.includes(x));
console.log(tab4)

// 2. Soit un tableau tab1=[5,3,87,1,-4,-99,0]

// 1. Ecrire une fonction qui  reçoit en parametre tab1 et qui permet de le trier
const tab1 = [5,3,87,1,-4,-99,0];
tab1.sort();
console.log(tab1);

// 2. Ecrire une fonction qui reçoit en parametre tab1 et retourne la valeur max , min
const tab1 = [5,3,87,1,-4,-99,0];
const MaxMin = (a, b) => b - a;
tab1.sort(MaxMin);
console.log(tab1);